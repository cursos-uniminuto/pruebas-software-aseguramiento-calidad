


<h3>Command line instructions</h3>

You can also upload existing files from your computer using the instructions below.
Git global setup

git config --global user.name "Jahir Saavedra" <br>
git config --global user.email "jahir.saavedra@uniminuto.edu" <br>

<h5>Create a new repository </h5>

git clone https://gitlab.com/cursos-uniminuto/pruebas-software-aseguramiento-calidad.git <br>
cd pruebas-software-aseguramiento-calidad <br>
git switch --create main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push --set-upstream origin main <br>

<h5>Push an existing folder </h5>

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/cursos-uniminuto/pruebas-software-aseguramiento-calidad.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push --set-upstream origin main <br>

<h5>Push an existing Git repository </h5>

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/cursos-uniminuto/pruebas-software-aseguramiento-calidad.git <br>
git push --set-upstream origin --all <br>
git push --set-upstream origin --tags <br>


